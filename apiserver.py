from langchain.llms import OpenAI
from langchain.chains.qa_with_sources import load_qa_with_sources_chain
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.text_splitter import CharacterTextSplitter
from langchain.vectorstores import Chroma
from langchain.document_loaders import TextLoader
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.chains import VectorDBQAWithSourcesChain

from flask import Flask, request
import json 

persist_directory = './db_new'
embedding = OpenAIEmbeddings()

# Now we can load the persisted database from disk, and use it as normal. 
vectordb = Chroma(persist_directory=persist_directory, embedding_function=embedding)
chain = VectorDBQAWithSourcesChain.from_chain_type(OpenAI(temperature=0), chain_type="map_reduce", vectorstore=vectordb)

print(vectordb)

# Setup flask server
app = Flask(__name__) 
  
@app.route('/ask', methods = ['GET']) 
def ask_result(): 
    args = request.args
    print(args.get("q"))
    # Return data in json format 
    query = args.get('q')
    # docs = vectordb.similarity_search_with_score(query)
    if query is not None:
        docs_from_search = vectordb.similarity_search(query)
        if len(docs_from_search)>0:
            print('Similarity Search Done')
            docs = chain({"input_documents": docs_from_search, "question": query}, return_only_outputs=True)
            print('Sending Result')
            return json.dumps(docs)
        else:
            return json.dumps({})
    else:
        return json.dumps({})
if __name__ == "__main__": 
    app.run(port=5000, debug=True)
